class_name MGDLIB_DialogBox
extends Node

enum BoxState {
	MODAL,
	NOT_MODAL,
	KEEP_AS_IT,
}

var is_initialized := false
var container: Node = null

var state := BoxState.NOT_MODAL:
	set(value):
		#print("CHANGING STATE:", value)
		state = value
	get:
		return state
var ok_text: String:
	set(value):
		ok_text = value
	get:
		return ok_text
var _accept_dialog: AcceptDialog
var _position_stored := Vector2i.ZERO


static func internal_error(error_code := MGdLib.ErrorCode.NOT_INITIALIZED) -> void:
	var message: String
	var dialog_name := "DialogBox"
	match error_code:
		MGdLib.ErrorCode.NOT_INITIALIZED:
			message = (
				"An instance of %s should have been created before acessing its members. Use DialogBox.new()."
				% dialog_name
			)
		MGdLib.ErrorCode.NO_CONTAINER:
			message = (
				"A container must be defined for %s. And it can'f be. Check the parameters of DialogBox.new()."
				% dialog_name
			)
		_:
			message = "An unknown error has occured for %s." % dialog_name
	print_debug(message)


func _init(container: Node, state = BoxState.MODAL, ok_text: String = "OK! I've got It"):
	init_dialog(container, state, ok_text)


func init_dialog(
	container_p: Node, state_p = BoxState.MODAL, ok_text_p: String = "OK! I've got It"
) -> void:
	is_initialized = false
	if container_p == null:
		internal_error(MGdLib.ErrorCode.NO_CONTAINER)
		return
	state = state_p
	ok_text = ok_text_p
	_accept_dialog = AcceptDialog.new()
	_accept_dialog.title = "Default AcceptDialog"
	_accept_dialog.set_text("This is the unchanged version")
	_accept_dialog.set_ok_button_text(ok_text)
	_accept_dialog.size = Vector2i(300, 100)
	_accept_dialog.exclusive = (state == BoxState.MODAL)
	container = container_p
	container.add_child.call_deferred(_accept_dialog)
	is_initialized = true


func message(
	message: String, level := MGdLib.LogLevel.INFO, state_p := BoxState.KEEP_AS_IT
) -> void:
	if not is_initialized:
		init_dialog(container, state, ok_text)

	var title := "Application "
	match level:
		#TODO: add some ascetic change depending on the type of box (different icon, color...)
		MGdLib.LogLevel.DEBUG:
			title += "Debug"
		MGdLib.LogLevel.INFO:
			title += "Info"
			Log.info(message)  # by default, the message is log as info. Other log action are done in caller functions
		MGdLib.LogLevel.WARN:
			title += "Warning"
		MGdLib.LogLevel.ERROR:
			title += "Error"
		MGdLib.LogLevel.FATAL:
			title += "FATAL"

	_accept_dialog.title = title
	_accept_dialog.set_text(message)
	_accept_dialog.set_ok_button_text(ok_text)
	if state_p != BoxState.KEEP_AS_IT:
		state = state_p
	show()


func close() -> void:
	if is_initialized:
		is_initialized = false
		container.remove_child(_accept_dialog)
		_accept_dialog.free()
	else:
		internal_error()


func show() -> void:
	if is_initialized:
		if state == BoxState.MODAL:
			#print("MODAL")
			_accept_dialog.set_flag(Window.FLAG_POPUP, false)
			_accept_dialog.exclusive = true
			show_in_center()
			await _accept_dialog.confirmed
		else:
			#print("NOT MODAL")
			_accept_dialog.set_flag(Window.FLAG_POPUP, true)
			_accept_dialog.exclusive = false
			#_accept_dialog.set_flag(Window.FLAG_ALWAYS_ON_TOP, true) not possible for a popup
			show_in_center()
			_accept_dialog.request_attention()
	else:
		internal_error()


func show_in_center():
	_accept_dialog.move_to_foreground()
	_accept_dialog.move_to_center()
	_accept_dialog.show()
	# fix re opening a closed windows does not appears on the center at first
	var pos := _accept_dialog.position
	if pos == Vector2i.ZERO:
		_accept_dialog.position = _position_stored
	else:
		_position_stored = pos


func hide() -> void:
	if is_initialized:
		_accept_dialog.hide()
	else:
		internal_error()


func debug(message: String) -> void:
	message(message, MGdLib.LogLevel.DEBUG)
	Log.dbg(message)


func warning(message: String) -> void:
	message(message, MGdLib.LogLevel.WARN)
	Log.warn(message)


func error(message: String) -> void:
	message(message, MGdLib.LogLevel.ERROR)
	await _accept_dialog.confirmed
	Log.error(message)


func fatal(message: String) -> void:
	message(message, MGdLib.LogLevel.FATAL)
	await _accept_dialog.confirmed
	Log.fatal(message)
	container.get_tree().quit(1)
