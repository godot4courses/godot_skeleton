@echo off
echo.
echo This script make all steps to init a new godot engine project.
echo You can launch the script in an existing godot project folder and its git repository will be reset and the addons defined in the plug.gd file will be updated or installed.
echo Or you can launch the script in an empty folder and the godot project will be cloned from a Godot skeleton repository.
echo.
echo NOTE:
echo If it's launched inside the folder of an existing project, It will reset the existing git repository.
echo And all existing git data will be lost.
echo The name of the project will be the name of folder.
echo.
rem this script:
rem - clones a godot project from a git repository or uses the current folder
rem - copies the scripts to the project folder
rem - reset the git repository
rem - update all addons using gd-plug
rem - open the project once to parse scripts and generate import files
rem - commit the addons changes to the repository

set GODOT_SKELETON_REPO=git@gitlab.com:godot4courses/godot_skeleton.git
set GODOT_CSSKELETON_REPO=git@gitlab.com:godot4courses/godotcs_skeleton.git
set PROJECT_FILE_NAME=project.godot
rem set to 1 to use if "%PAUSE_DURING_PROCESS%"=="1" pause during the process
set PAUSE_DURING_PROCESS=1

rem set the path to the godot executable
rem set GODOT_EXE=C:\Program Files\Godot\godot.exe
REM get the godot location from the path environment variable
for /f "delims=" %%i in ('where godot') do set "GODOT_EXE=%%i"

if not exist "%GODOT_EXE%" (
  echo Godot executable not found in the path environment variable
  echo.
  rem ask the use for the godot location
  set /p "GODOT_EXE=Please enter the path to the godot executable: "
)
if not exist "%GODOT_EXE%" (
  echo Godot executable not found
  goto :END
)

:INIT
set "PROJECT_FOLDER=%CD%"
set "ADDONS_FOLDER=%PROJECT_FOLDER%\addons"
set "SCRIPTS_FOLDER=%ADDONS_FOLDER%\_scripts"

echo.
echo Scripts parameters
echo.
echo Godot executable: %GODOT_EXE%
echo project folder: %PROJECT_FOLDER%
echo addons folder: %ADDONS_FOLDER%
echo scripts folder: %SCRIPTS_FOLDER%
echo.

:ASK_USER
rem ask the user to clone an existing git repo
echo What do you want to do for the project:
echo 1 only Update addons only in the current folder
echo 2 only Clean cache in the current folder
echo 3 update the godot project in the current folder
echo 4 Create a new project using The Godot skeleton
echo 5 Create a new project using The Godot C# skeleton
echo 6 Only clone The Godot skeleton
echo 7 Only clone The Godot CS skeleton
echo 8 Exit
echo.
choice /c 12345678 /n /m "Enter the number of the choosen action: "
if "%errorlevel%"=="1" (
  call GodotUpdateAddons.bat "%PROJECT_FOLDER%" "%GODOT_EXE%"
  goto :DONE
) else if "%errorlevel%"=="2" (
  call GodotCleanCache.bat "%PROJECT_FOLDER%""
  goto :DONE
) else if "%errorlevel%"=="3" (
  set "PROJECT_REPO=."
  rem the next value is by default
  set "REPO_NAME=GodotSkeleton"
) else if "%errorlevel%"=="4" (
  set "PROJECT_REPO=%GODOT_SKELETON_REPO%"
  set "REPO_NAME=GodotSkeleton"
  set CLONE_ONLY=0
) else if "%errorlevel%"=="5" (
  set "PROJECT_REPO=%GODOT_CSSKELETON_REPO%"
  set "REPO_NAME=GodotSkeletonCS"
  set CLONE_ONLY=0
) else if "%errorlevel%"=="6" (
  set "PROJECT_REPO=%GODOT_SKELETON_REPO%"
  set REPO_NAME=GodotSkeleton
  set CLONE_ONLY=1
) else if "%errorlevel%"=="7" (
  set "PROJECT_REPO=%GODOT_CSSKELETON_REPO%"
  set "REPO_NAME=GodotSkeletonCS"
  set CLONE_ONLY=1
) else (
  goto :DONE
)

echo.
if "%PROJECT_REPO%"=="." (
  echo The current folder will be used as the source
) else (
  dir /a /b | findstr .. >nul
  if errorlevel 1 (
    cd /D "%PROJECT_FOLDER%"
    git clone "%PROJECT_REPO%" .
    if "%CLONE_ONLY%"=="1" (
      goto :DONE
    )
  ) else (
    echo The folder %CD% is NOT empty
    echo You can't clone a repo inside this folder. Please choose another folder or delete its content
    echo and run the script again.
    goto :DONE
  )
  echo The project will be cloned from the repository %PROJECT_REPO%
)
echo.

:CHECK
echo.
echo ###################
echo Checks
echo ###################
echo.
rem check if the current folder contains a godot project
if not exist "%PROJECT_FILE_NAME%" (
  echo The folder %PROJECT_FOLDER% does not contains a godot project
  echo This script must be run inside an existing godot project folder or you must choose to clone a repo when asked for
  goto :DONE
)

rem create the scripts folder if it doesn't exist
if not exist "%SCRIPTS_FOLDER%" (
  mkdir "%SCRIPTS_FOLDER%"
)
echo.
echo Copying used scripts into %SCRIPTS_FOLDER%
echo.
copy "%0" "%SCRIPTS_FOLDER%" >nul
copy "%~dp0\GodotCleanCache.bat" "%SCRIPTS_FOLDER%" >nul
copy "%~dp0\GodotUpdateAddons.bat" "%SCRIPTS_FOLDER%" >nul

:GIT_RESET
echo.
echo ###################
echo Git reset
echo ###################
echo.
if "%PAUSE_DURING_PROCESS%"=="1" pause
cd /D "%PROJECT_FOLDER%"
if exist .git (
	rmdir /s /q .git
)
git init
git-lfs install
git add .
git commit -m "Initial commit"

:CLEAN_CACHE
rem run the GodotCleanCache.bat script
call "%SCRIPTS_FOLDER%/GodotCleanCache.bat" "%PROJECT_FOLDER%"

:ADDONS_UPDATE
rem run the GodotCleanCache.bat script
call "%SCRIPTS_FOLDER%/GodotUpdateAddons.bat" "%PROJECT_FOLDER%" "%GODOT_EXE%"

:OPEN_PROJECT
echo.
echo ---------
echo Open the project once to parse scripts and generate import files
echo ---------
echo.
rem open the project and quit after 1000 frames (about 20 seconds)
"%GODOT_EXE%" -e --quit-after 1000 --quiet --path "%PROJECT_FOLDER%"
rem open the project and quit
rem %GODOT_EXE% -e --quit --quiet --path .

:COMMIT_CHANGES
echo.
echo ###################
echo Commit addons changes to the repository
echo ###################
echo.
if "%PAUSE_DURING_PROCESS%"=="1" pause
cd /D "%PROJECT_FOLDER%"
git add .
git commit -m "project initialisation and addons update"

:RENAME_PROJECT
echo.
echo ###################
echo Change project name
echo ###################
echo.
if "%PAUSE_DURING_PROCESS%"=="1" pause
cd /D "%PROJECT_FOLDER%"
rem copy %PROJECT_FILE_NAME% %PROJECT_FILE_NAME%.bak
set search=%REPO_NAME%
rem get the name of the current folder with its path
for %%i in ("%PROJECT_FOLDER%") do set replace=%%~nxi
set "textfile=%PROJECT_FILE_NAME%"
rem we use python because the batch language is not easy to do this
python -c "fi=open('./%textfile%', 'r'); content=fi.read(); fi.close(); fo=open('./%textfile%', 'w'); fo.write(content.replace('%search%', '%replace%')); fo.close()"

:DONE
echo.
echo ###################
echo DONE
echo ###################
echo.