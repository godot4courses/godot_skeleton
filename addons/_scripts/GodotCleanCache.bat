@echo off
rem this script:
rem - removes unused files and folders from the .godot folder in a godot project
rem - removes the demo folder if it exists
rem - copies some files to be preserved before cleaning cache
rem - restores the preserved files after cleaning cache

rem usage: GodotCleanCache.bat [project_folder]
rem if no project_folder is provided, the current folder is used

set "PROJECT_FOLDER=%~1"
if "%PROJECT_FOLDER%"=="" set "PROJECT_FOLDER=%CD%"
if not exist "%PROJECT_FOLDER%" (
  echo.
  echo Project folder not found
  goto :END
)

if not exist "%PROJECT_FOLDER%\.godot" (
	echo.
  echo .godot folder not found in the project. No clean will be done
  goto :END
)

echo.
echo ---------
echo Remove unused file or folders
echo ---------
echo.
if exist "%PROJECT_FOLDER%\demo" (
	rmdir /s /q "%PROJECT_FOLDER%\demo"
)

rem copy some files to be preserved before cleaning cache
copy "%PROJECT_FOLDER%\.godot\editor\editor_layout.cfg" .
copy "%PROJECT_FOLDER%\.godot\editor\project_metadata.cfg" .
copy "%PROJECT_FOLDER%\.godot\extension_list.cfg" .
copy "%PROJECT_FOLDER%\.godot\.gdignore" .

rem clean cache (remove files only)
del /f /s /q "%PROJECT_FOLDER%\.godot\*"

rem restore preserved files
move .\editor_layout.cfg "%PROJECT_FOLDER%\.godot\editor\"
move .\project_metadata.cfg "%PROJECT_FOLDER%\.godot\editor\"
move .\extension_list.cfg "%PROJECT_FOLDER%\.godot\"
move .\.gdignore "%PROJECT_FOLDER%\.godot\"

:END