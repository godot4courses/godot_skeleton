@echo off
rem this script:
rem - updates all addons using gd-plug

rem usage:
rem GodotUpdateAddons.bat [project_folder] [godot_exe_full_path]
rem if no project_folder is provided, the current folder is used
rem if no godot_exe_full_path is provided, the godot command found in path is used

set "PROJECT_FOLDER=%~1"
if "%PROJECT_FOLDER%"=="" set "PROJECT_FOLDER=%CD%"
if not exist "%PROJECT_FOLDER%" (
  echo.
  echo Project folder not found
  goto :END
)

set "GODOT_EXE=%~2"
if "%GODOT_EXE%"=="" (
  set "GODOT_EXE=godot"
) else (
  if not exist "%GODOT_EXE%" (
    echo.
    echo Godot executable not found
    goto :END
  )
)
pushd "%PROJECT_FOLDER%"
rem check if the project is using gd-plug
if not exist "addons/gd-plug" (
	echo.
  echo gd-plug not found in the project. No update will be done
  goto :END
) else (
  if exist "plug.gd" (
    echo.
    echo ###################
    echo Update all addons using gd-plug
    echo ###################
    pause

    echo.
    echo ---------
    echo Upgrade addons/gd-plug/plug.gd to the latest version
    echo ---------
    echo.
    "%GODOT_EXE%" --headless -s plug.gd upgrade

    echo.
    echo ---------
    echo Clean unused files/folders from /.plugged
    echo ---------
    echo.
    "%GODOT_EXE%" --headless -s plug.gd clean

    echo.
    echo ---------
    echo Install or update to latest version plugins based on plug.gd, or uninstall unplugged plugins
    echo ---------
    echo.
    "%GODOT_EXE%" --headless -s plug.gd update force
  )
)

:END
popd
